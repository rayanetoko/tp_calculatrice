let box = null;
let history = null;
let lastNumber = null;

document.addEventListener('DOMContentLoaded', function() {

    box = document.getElementById("box");
    box.textContent = '0';

    history = document.getElementById('last_operation_history');

})

function button_number(x) {
    if (box.textContent === '0' || history.textContent.length > 0) {
        box.textContent = x;
    } else {
        box.textContent += x;
    }
    if (history.textContent.length > 0) {
        history.textContent = '';
    }
}

function backspace_remove() {
    box.textContent = box.textContent.substring(0, box.textContent.length - 1);

    if (box.textContent.length === 0)
        box.textContent = '0';
}

function button_clear () {
    box.textContent = '0';
    history.textContent = '0'
}

function clear_entry() {
    box.textContent = '0';
}

function calculate_percentage() {
    const result = parseFloat(box.textContent) / 100;
    display(result, `${parseFloat(box.textContent)}%`);
}

function division_one() {
    const result = 1 / parseFloat(box.textContent);
    display(result, `1/${parseFloat(box.textContent)}`);
}

function power_of() {
    const result = Math.pow(parseFloat(box.textContent), 2);

    display(result, `${box.textContent}²`);
}

function square_root() {
    const result = Math.sqrt(parseFloat(box.textContent));

    display(result, `V${parseFloat(box.textContent)}`)
}

function display(result, historyFormat) {
    box.textContent = result;
    history.textContent = historyFormat + " =";
}